package main

import (
	"encoding/json"
	"fmt"
	"os/exec"
	"strings"
	"os"
	"flag"
	"strconv"
	"io"
	"log"
	"bytes"

)

//////////////
/// MODEL ///
////////////

type duration struct {
	Ticks int `json:"Ticks"`
}

type chapter struct {
	Name string `json:"Name"`
	Duration duration `json:"Duration"`
}

type title struct {
	// ChapterList []chapter `json:"ChapterList"`
	Duration duration `json:"Duration"`
	Name string `json:"Name"`
	Index int `json:"Index"`
}

type title_set struct {
	TitleList []title `json:"TitleList"`
}

func GetTitles(device string) title_set {
	cmd := exec.Command("HandBrakeCLI", "-i", device, "--scan", "--json", "-t", "0")
	out, _ := cmd.Output()
	title_set_text := strings.Split(string(out), "JSON Title Set: ")[1]
	fmt.Printf("out %s", title_set_text)

	f, _ := os.Create("/tmp/c.json")
	f.WriteString(title_set_text)

	titleSet := title_set{}
	err := json.Unmarshal([]byte(title_set_text), &titleSet)
	if err != nil {
		fmt.Println(err)
	}
	return titleSet
}

func main() {

	var output_folder string
	var device string
	flag.StringVar(&output_folder, "output", "", "Folder to output to")
	flag.StringVar(&device, "device", "", "Usually /dev/sr0 or /dev/sr1")
	flag.Parse()
	fmt.Println("Scanning tracks...")

	titles := GetTitles(device)
	fmt.Println(titles)
	for _, title := range titles.TitleList {
		index := strconv.Itoa(title.Index)
		output_file := output_folder + "/" + title.Name + "-Track" + index + ".mkv"
		fmt.Printf("Rip Title %s of %d to %s", index, len(titles.TitleList), output_file)
		cmd := exec.Command("HandBrakeCLI", 
			"-i", device, 
			"-t", index, 
			"--audio-lang-list", `"deu, eng"`,
			"--min-duration", "300",
			"--all-audio",
			"--subtitle-lang-list", `"deu, eng"`,
			"--encoder", "x264", 
			"--encoder-profile", "high", 
			"--encoder-preset", "medium",
			"--encoder-level", "4.1",
			"--quality", "21",
			"--optimize",
			"--all-subtitle",
			"-o", output_file)

		// Run command and print progress to stdout
		var stdBuffer bytes.Buffer
		mw := io.MultiWriter(os.Stdout, &stdBuffer)
		
		cmd.Stdout = mw
		cmd.Stderr = mw
		
		if err := cmd.Run(); err != nil {
				log.Panic(err)
		}
		
		log.Println(stdBuffer.String())
	}
	
	exec.Command("eject", device)
}